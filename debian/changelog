pulseaudio-modules-droid-jb2q (14.2.102-0ubports1) noble; urgency=medium

  * New upstream version 14.2.102.
  * d/patches: refresh and forward-port all patches to the new version
  * Fold a couple of patches into the relevant patches
    - 2001-Fix-pkgconf-for-debian-based-systems.patch: fold into
      2008-ubports-correct-macro-usage-in-pc-files.patch
    - 2006-extevdev-make-the-combo-port-always-available.patch: fold into
      2004-card-read-headphone-availability-from-input-device.patch
    - 2009-sink-move-fake-SCO-property-setting-to-a-hook-instea.patch: fold
      into 2005-add-support-for-SCO-fake-sink-suspend-prevention.patch
  * d/patches: correct library path in .pc files
    - 1004-meson.build-correctly-construct-modlibexecdir-from-o.patch
  * debian/: update packaging for Meson build system

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 03 Sep 2024 03:03:18 +0700

pulseaudio-modules-droid-jb2q (14.2.97-0ubports1) focal; urgency=medium

  * Rename to pulseaudio-modules-droid-jb2q following upstream
  * Update to version 14.2.97
  * Rebase patches
  * Fix homepage and add VCS-URLs
  * Update to dh version 12

 -- Guido Berhoerster <guido+gitlab.com@berhoerster.name>  Tue, 31 May 2022 14:36:18 +0200

pulseaudio-modules-droid (14.2.92-0ubports2) focal; urgency=medium

  * debian/patches: fix mistake in re-basing the patches
    + 2004-card-read-headphone-availability-from-input-device.patch: re-add
      the source file into Makefile.am

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Sat, 11 Sep 2021 01:35:41 +0700

pulseaudio-modules-droid (14.2.92-0ubports1) focal; urgency=medium

  * New upstream version
  * Move Jenkinsfile to debian/ per the new guideline
  * Switch to '3.0 (quilt)'; add source location
  * debian/rules:
    + don't hardcode pulseaudio version
    + be more sensitive to failures in loops
    + make available .tarball-version
    + create .tarball-version before autoreconf
    + force the deletion of .tarball-version
    + match how Debian's Pulseaudio pkg defines module-dir
  * debian/patches:
    + import patches from xenial_-_android9
    + Correct Pulseaudio detection in the configure script
    + Correct module dir in .pc file

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Sat, 11 Sep 2021 00:11:08 +0700

pulseaudio-modules-droid (12.2.84) xenial; urgency=medium

  * Import Mer upstream.
  * Compile against android-headers-28 for Halium 9.

 -- TheKit <nekit1000@gmail.com>  Mon, 23 Mar 2020 01:49:13 +0100

pulseaudio-modules-droid (11.1.76) xenial; urgency=medium

  * Sync with Mer upstream.

 -- TheKit <nekit1000@gmail.com>  Sun, 24 Feb 2019 22:11:55 +0100

pulseaudio-modules-droid (0.1) xenial; urgency=medium

  * Initial release.

 -- Marius Gripsgard <marius@ubports.com>  Fri, 19 Jan 2018 07:39:13 +0100
